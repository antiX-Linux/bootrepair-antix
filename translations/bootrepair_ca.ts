<?xml version="1.0" ?><!DOCTYPE TS><TS language="ca" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="ui_mainwindow.h" line="392"/>
        <source>Boot Repair</source>
        <translation>Repara l&apos;arrencada </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="ui_mainwindow.h" line="393"/>
        <source>Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Boot Repair és una utilitat que es pot usar per a reinstal·lar el carregador GRUB als sitemes ESP (EFI System Partition), MBR (Master Boot Record) o a la partició arrel. Dóna l&apos;opció de reconstruir el fitxer de configuració del GRUB i fer còpia de seguretat o restaurar el MBR o PBR (arrel).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <location filename="ui_mainwindow.h" line="394"/>
        <source>What would you like to do?</source>
        <translation>Què voldríeu fer?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <location filename="ui_mainwindow.h" line="395"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Còpia de seguretat del MBR o PBR (només arrencada antiga)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <location filename="ui_mainwindow.h" line="396"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Reinstal·la el carregador GRUB en ESP, MBR o PBR (root)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="ui_mainwindow.h" line="397"/>
        <source>Repair GRUB configuration file</source>
        <translation>Repara el fitxer de configuració del GRUB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="ui_mainwindow.h" line="398"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Restaura el MBR o PBR de la còpia de seguretat (només arrencada antiga)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <location filename="mainwindow.cpp" line="483"/>
        <location filename="ui_mainwindow.h" line="399"/>
        <source>Select Boot Method</source>
        <translation>Trieu el mètode d&apos;arrencada</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="ui_mainwindow.h" line="401"/>
        <source>Master Boot Record</source>
        <translation>Registre d&apos;Arrencada Principal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="ui_mainwindow.h" line="403"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.ui" line="430"/>
        <location filename="ui_mainwindow.h" line="405"/>
        <location filename="ui_mainwindow.h" line="426"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="408"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Arrel (Registre d&apos;Arrencada de Partició)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <location filename="mainwindow.cpp" line="486"/>
        <location filename="ui_mainwindow.h" line="410"/>
        <source>root</source>
        <translation>Arrel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="ui_mainwindow.h" line="411"/>
        <source>Install on:</source>
        <translation>Instal·la a: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <location filename="mainwindow.cpp" line="484"/>
        <location filename="ui_mainwindow.h" line="412"/>
        <source>Location:</source>
        <translation>Ubicació: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <location filename="mainwindow.cpp" line="494"/>
        <location filename="ui_mainwindow.h" line="416"/>
        <source>Select root location:</source>
        <translation>trieu la ubicació de l&apos;arrel:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="418"/>
        <source>EFI System Partition</source>
        <translation>Partició de Sistema EFI </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <location filename="ui_mainwindow.h" line="420"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <location filename="ui_mainwindow.h" line="422"/>
        <source>About this application</source>
        <translation>Quant a aquest programa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <location filename="ui_mainwindow.h" line="424"/>
        <source>About...</source>
        <translation>Quant a...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="429"/>
        <source>Display help </source>
        <translation>Mostra l&apos;ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <location filename="ui_mainwindow.h" line="431"/>
        <source>Help</source>
        <translation>Ajuda </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancel·la els canvis i surt </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="533"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Apply any changes</source>
        <translation>Aplica els canvis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="555"/>
        <location filename="mainwindow.cpp" line="71"/>
        <location filename="ui_mainwindow.h" line="446"/>
        <source>Apply</source>
        <translation>Aplica</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>S&apos;està instal·lant GRUB al dispositiu %1 .</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="216"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="396"/>
        <location filename="mainwindow.cpp" line="419"/>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Could not create a temporary folder</source>
        <translation>No s&apos;ha pogut crear un directori temporal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>No es pot establir l&apos;entorn de chroot.
Si us plau, verifiqueu la ubicació triada. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>S&apos;està reconstruint el fitxer de configuració del GRUB (grub.cfg).</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Fent còpia de seguretat del MBR o PBR del dispositiu %1 .</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Warning</source>
        <translation>Atenció </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>You are going to write the content of </source>
        <translation>Ara s&apos;enregistrarà el contingut de </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source> to </source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>

Are you sure?</source>
        <translation>

N&apos;esteu segurs? </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Restaurant el MBR/PBR de la còpia de seguretat al dispositiu %1 .</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>No trobo la partició EFI de sistema (ESP) a cap disc del sistema. Si us plau, creeu un ESP i torneu a provar-ho.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Select %1 location:</source>
        <translation>Trieu la ubicació de %1 :</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Back</source>
        <translation>Enrere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Success</source>
        <translation>Èxit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Boot Repair?&lt;/b&gt;</source>
        <translation>Procés acabat amb èxit.&lt;p&gt;&lt;b&gt;Voleu sortir de Boot Repair?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="396"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>El procés ha acabat. Hi ha hagut errors.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Entreu la contrasenya per desbloquejar la partició encriptada %1:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Ho sento, no puc configurar el contenidor LUKS %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Select GRUB location</source>
        <translation>Trieu la ubicació de GRUB </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Select Item to Back Up</source>
        <translation>Trieu l&apos;element per fer còpia de seguretat</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Select Item to Restore</source>
        <translation>Trieu l&apos;element a restaurar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>Select backup file name</source>
        <translation>Trieu el nom del fitxer de còpia de seguretat </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>No file was selected.</source>
        <translation>No s&apos;ha triat cap fitxer. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Trieu un fitxer de còpia de seguretat de MBR o PBR </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="550"/>
        <source>About %1</source>
        <translation>Quant a %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>Versió: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Simple boot repair program for antiX Linux</source>
        <translation>Programa senzill per reparar l&apos;arrencada a antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="554"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>%1 License</source>
        <translation>Llicència de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>%1 Help</source>
        <translation>Ajuda de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Ho sento, no puc muntar la partició %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Llicència</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Registre de canvis</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>Tan&amp;ca </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Sembla que esteu connectat com a administrador, si us plau sortiu i connecteu-vos com a usuari normal per a usar aquest programa.</translation>
    </message>
</context>
</TS>