<?xml version="1.0" ?><!DOCTYPE TS><TS language="pl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="ui_mainwindow.h" line="392"/>
        <source>Boot Repair</source>
        <translation>Naprawa rozruchu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="ui_mainwindow.h" line="393"/>
        <source>Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Naprawa rozruchu to narzędzie, które może być użyte do ponownego zainstalowania programu rozruchowego GRUB na ESP (partycji systemowej EFI), MBR (Master Boot Record) lub partycji root. Zapewnia opcję odtworzenia pliku konfiguracyjnego GRUB oraz utworzenia kopii zapasowych i przywracania MBR lub PBR (root).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <location filename="ui_mainwindow.h" line="394"/>
        <source>What would you like to do?</source>
        <translation>Co chciałbyś zrobić?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <location filename="ui_mainwindow.h" line="395"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Wykonaj kopię zapasową MBR lub PBR (tylko starsze komputery)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <location filename="ui_mainwindow.h" line="396"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Zainstaluj ponownie program rozruchowy GRUB na ESP, MBR lub PBR (root)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="ui_mainwindow.h" line="397"/>
        <source>Repair GRUB configuration file</source>
        <translation>Napraw plik konfiguracyjny GRUB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="ui_mainwindow.h" line="398"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Przywróć MBR lub PBR z kopii zapasowej (tylko starsza wersja rozruchowa)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <location filename="mainwindow.cpp" line="483"/>
        <location filename="ui_mainwindow.h" line="399"/>
        <source>Select Boot Method</source>
        <translation>Wybierz metodę rozruchu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="ui_mainwindow.h" line="401"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record (główny sektor rozruchowy)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="ui_mainwindow.h" line="403"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.ui" line="430"/>
        <location filename="ui_mainwindow.h" line="405"/>
        <location filename="ui_mainwindow.h" line="426"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="408"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Root (Partycja rozruchowa)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <location filename="mainwindow.cpp" line="486"/>
        <location filename="ui_mainwindow.h" line="410"/>
        <source>root</source>
        <translation>root</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="ui_mainwindow.h" line="411"/>
        <source>Install on:</source>
        <translation>Zainstaluj na:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <location filename="mainwindow.cpp" line="484"/>
        <location filename="ui_mainwindow.h" line="412"/>
        <source>Location:</source>
        <translation>Lokalizacja:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <location filename="mainwindow.cpp" line="494"/>
        <location filename="ui_mainwindow.h" line="416"/>
        <source>Select root location:</source>
        <translation>Wybierz lokalizację katalogu głównego root:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="418"/>
        <source>EFI System Partition</source>
        <translation>Partycja systemowa EFI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <location filename="ui_mainwindow.h" line="420"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <location filename="ui_mainwindow.h" line="422"/>
        <source>About this application</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <location filename="ui_mainwindow.h" line="424"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="429"/>
        <source>Display help </source>
        <translation>Wyświetl pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <location filename="ui_mainwindow.h" line="431"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Cancel any changes then quit</source>
        <translation>Anuluj wszelkie zmiany, a następnie zakończ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="533"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Apply any changes</source>
        <translation>Zastosuj wszystkie zmiany</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="555"/>
        <location filename="mainwindow.cpp" line="71"/>
        <location filename="ui_mainwindow.h" line="446"/>
        <source>Apply</source>
        <translation>Zastosuj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>GRUB jest instalowany na urządzeniu %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="216"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="396"/>
        <location filename="mainwindow.cpp" line="419"/>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Could not create a temporary folder</source>
        <translation>Nie można utworzyć folderu tymczasowego</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Błąd tworzenia środowiska chroot
Proszę sprawdź wybraną lokalizację docelową.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>Plik konfiguracyjny GRUB (grub.cfg) jest przebudowywany.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Tworzenie kopii zapasowej MBR lub PBR z urządzenia %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>You are going to write the content of </source>
        <translation>Za chwilę nastąpi zapisanie zawartości </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source> to </source>
        <translation>do</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>

Are you sure?</source>
        <translation>

Jesteś pewien?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Przywracanie MBR/PBR z kopii zapasowej na urządzenie %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>Nie można znaleźć partycji systemowej EFI (ESP) na żadnym dysku systemowym. Utwórz ESP i spróbuj ponownie.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Select %1 location:</source>
        <translation>Wybierz lokalizację %1 :</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Back</source>
        <translation>Wstecz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Success</source>
        <translation>Sukces </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Boot Repair?&lt;/b&gt;</source>
        <translation>Proces zakończył się sukcesem.&lt;p&gt;&lt;b&gt;Czy chcesz opuścić program Naprawa rozruchu?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="396"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>Proces zakończony. Wystąpiły błędy.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Wprowadź hasło, aby odblokować zaszyfrowaną partycję %1 :</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Niestety, nie można otworzyć kontenera LUKS %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Select GRUB location</source>
        <translation>Wybierz lokalizację GRUB</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Select Item to Back Up</source>
        <translation>Wybierz element do utworzenia kopii zapasowej</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Select Item to Restore</source>
        <translation>Wybierz element do przywrócenia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>Select backup file name</source>
        <translation>Wybierz nazwę pliku kopii zapasowej</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>No file was selected.</source>
        <translation>Nie wybrano pliku.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Wybierz plik kopii zapasowej MBR lub PBR</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="550"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>Wersja:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Simple boot repair program for antiX Linux</source>
        <translation>Prosty program do naprawy rozruchu dla dystrybucji antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="554"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Prawa autorskie © MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>%1 License</source>
        <translation>%1 Licencja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>%1 Help</source>
        <translation>%1 Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Niestety, nie można zamontować partycji %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Dziennik zmian</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Wygląda na to, że jesteś zalogowany jako root, wyloguj się i zaloguj jako zwykły użytkownik, aby korzystać z tego programu.</translation>
    </message>
</context>
</TS>