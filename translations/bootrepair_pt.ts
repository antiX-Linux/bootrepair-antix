<?xml version="1.0" ?><!DOCTYPE TS><TS language="pt" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="ui_mainwindow.h" line="392"/>
        <source>Boot Repair</source>
        <translation>Reparador do Arranque</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="ui_mainwindow.h" line="393"/>
        <source>Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>O Reparador do Arranque é um utilitário que pode ser usado para reinstalar o carregador do arranque GRUB na ESP (EFI Sistema Partition), no MBR (Master Boot Record) do disco ou no PBR (Partition Boot Record) da partição raiz ( / , root), para reconstruir o ficheiro de configuração do GRUB, para fazer cópias de segurança do MBR ou do PBR, ou para restaurar o MBR ou o PBR.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <location filename="ui_mainwindow.h" line="394"/>
        <source>What would you like to do?</source>
        <translation>O que fazer?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <location filename="ui_mainwindow.h" line="395"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Cópia de segurança do MBR ou do PBR (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <location filename="ui_mainwindow.h" line="396"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Reinstalar o carregador do arranque GRUB em ESP, MBR ou PBR (root)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="ui_mainwindow.h" line="397"/>
        <source>Repair GRUB configuration file</source>
        <translation>Refazer o ficheiro de configuração do GRUB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="ui_mainwindow.h" line="398"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Restaurar MBR ou PBR a partir de cópia (apenas em computadores com BIOS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <location filename="mainwindow.cpp" line="483"/>
        <location filename="ui_mainwindow.h" line="399"/>
        <source>Select Boot Method</source>
        <translation>Dados para instalação do Arranque</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="ui_mainwindow.h" line="401"/>
        <source>Master Boot Record</source>
        <translation>Master Boot Record</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="ui_mainwindow.h" line="403"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.ui" line="430"/>
        <location filename="ui_mainwindow.h" line="405"/>
        <location filename="ui_mainwindow.h" line="426"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="408"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Raíz (Partition Boot Record)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <location filename="mainwindow.cpp" line="486"/>
        <location filename="ui_mainwindow.h" line="410"/>
        <source>root</source>
        <translation>PBR (da partição raíz)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="ui_mainwindow.h" line="411"/>
        <source>Install on:</source>
        <translation>Instalar em:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <location filename="mainwindow.cpp" line="484"/>
        <location filename="ui_mainwindow.h" line="412"/>
        <source>Location:</source>
        <translation>Localização:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <location filename="mainwindow.cpp" line="494"/>
        <location filename="ui_mainwindow.h" line="416"/>
        <source>Select root location:</source>
        <translation>Seleccionar a localização da raiz ( / , root):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="418"/>
        <source>EFI System Partition</source>
        <translation>Partição de Sistema EFI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <location filename="ui_mainwindow.h" line="420"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <location filename="ui_mainwindow.h" line="422"/>
        <source>About this application</source>
        <translation>Sobre esta aplicação</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <location filename="ui_mainwindow.h" line="424"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="429"/>
        <source>Display help </source>
        <translation>Mostrar ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <location filename="ui_mainwindow.h" line="431"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancelar alterações e sair</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="533"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Apply any changes</source>
        <translation>Aplicar alterações</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="555"/>
        <location filename="mainwindow.cpp" line="71"/>
        <location filename="ui_mainwindow.h" line="446"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>O GRUB está a ser instalado em %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="216"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="396"/>
        <location filename="mainwindow.cpp" line="419"/>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Could not create a temporary folder</source>
        <translation>Não foi possível criar uma pasta temporária</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>Não foi possível definir o ambiente chroot.
Verificar novamente a localização seleccionada.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>O ficheiro de configuração do GRUB (grub.cfg) está a ser refeito.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>A guardar o MBR/PBR de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>You are going to write the content of </source>
        <translation>Vai ser escrito o conteúdo de </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source> to </source>
        <translation> em </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>

Are you sure?</source>
        <translation>
    
Seguramente?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>A restaurar o MBR/PBR em %1 a partir da cópia.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>Não foi encontrada qualquer ESP (EFI System Partition). Criar uma partição ESP e voltar a tentar.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Select %1 location:</source>
        <translation>Seleccionar a localização de %1:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Back</source>
        <translation>Voltar atrás</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Success</source>
        <translation>Êxito</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Boot Repair?&lt;/b&gt;</source>
        <translation>O processo terminou com êxito.&lt;p&gt;&lt;b&gt;Sair do Reparador do Arranque?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="396"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>O processo terminou. Ocorreram erros.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Introduzir a senha para desbloquear a partição encriptada %1:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Não foi possível abrir o contentor LUKS %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Select GRUB location</source>
        <translation>Seleccionar a localização do GRUB</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Select Item to Back Up</source>
        <translation>Seleccionar o item a Copiar para Segurança</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Select Item to Restore</source>
        <translation>Seleccionar o item a Restaurar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>Select backup file name</source>
        <translation>Escolher o nome da cópia de segurança.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>No file was selected.</source>
        <translation>Não foi seleccionado qualquer ficheiro.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Seleccionar cópia de segurança do MBR ou do PBR</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="550"/>
        <source>About %1</source>
        <translation>Sobre o %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>Versão: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Simple boot repair program for antiX Linux</source>
        <translation>Programa simples de reparação do arranque para o antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="554"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Direitos de autor (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>%1 License</source>
        <translation>Licença %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>%1 Help</source>
        <translation>%1 Ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Não foi possível montar a partição %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Registo de alterações</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>O utilizador parece ser o root; para usar este programa, sair e voltar a entrar como utilizador normal.</translation>
    </message>
</context>
</TS>