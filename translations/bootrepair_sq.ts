<?xml version="1.0" ?><!DOCTYPE TS><TS language="sq" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="ui_mainwindow.h" line="392"/>
        <source>Boot Repair</source>
        <translation>Riparim Nisjeje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="ui_mainwindow.h" line="393"/>
        <source>Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Riparim Nisjeje është një mjet që mund të përdoret për të riinstaluar ngarkuesin GRUB të nisjeve në pjesën ESP (EFI System Partition), MBR (Master Boot Record) ose rrënjë. Ofron mundësinë të rindërtohet kartela e formësimit GRUB dhe të kopjeruhet dhe rikthehet në punë MBR-i ose PBR-i (rrënjë).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <location filename="ui_mainwindow.h" line="394"/>
        <source>What would you like to do?</source>
        <translation>Ç’do të donit të bëni?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <location filename="ui_mainwindow.h" line="395"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Kopjeruaj MBR ose PBR (vetëm për nisje si dikur)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <location filename="ui_mainwindow.h" line="396"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Riinstalo ngarkuesin GRUB të nisjeve në ESP, MBR ose PBR (rrënjë)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="ui_mainwindow.h" line="397"/>
        <source>Repair GRUB configuration file</source>
        <translation>Riparoni kartelë formësimi GRUB-i</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="ui_mainwindow.h" line="398"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Rikthe MBR ose PBR prej kopjeruajtjeje (vetëm për nisje si dikur)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <location filename="mainwindow.cpp" line="483"/>
        <location filename="ui_mainwindow.h" line="399"/>
        <source>Select Boot Method</source>
        <translation>Përzgjidhni Metodë Nisjeje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="ui_mainwindow.h" line="401"/>
        <source>Master Boot Record</source>
        <translation>Regjistër i Përgjithshëm Nisjesh</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="ui_mainwindow.h" line="403"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.ui" line="430"/>
        <location filename="ui_mainwindow.h" line="405"/>
        <location filename="ui_mainwindow.h" line="426"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="408"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Rrënjë (Regjistër Nisjeje i Pjesës)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <location filename="mainwindow.cpp" line="486"/>
        <location filename="ui_mainwindow.h" line="410"/>
        <source>root</source>
        <translation>rrënjë</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="ui_mainwindow.h" line="411"/>
        <source>Install on:</source>
        <translation>Instaloje në:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <location filename="mainwindow.cpp" line="484"/>
        <location filename="ui_mainwindow.h" line="412"/>
        <source>Location:</source>
        <translation>Vend:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <location filename="mainwindow.cpp" line="494"/>
        <location filename="ui_mainwindow.h" line="416"/>
        <source>Select root location:</source>
        <translation>Përzgjidhni vendndodhje rrënje:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="418"/>
        <source>EFI System Partition</source>
        <translation>Pjesë EFI Sistemi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <location filename="ui_mainwindow.h" line="420"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <location filename="ui_mainwindow.h" line="422"/>
        <source>About this application</source>
        <translation>Mbi këtë aplikacion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <location filename="ui_mainwindow.h" line="424"/>
        <source>About...</source>
        <translation>Mbi…</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="429"/>
        <source>Display help </source>
        <translation>Shfaq ndihmë</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <location filename="ui_mainwindow.h" line="431"/>
        <source>Help</source>
        <translation>Ndihmë</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Alt+H</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Cancel any changes then quit</source>
        <translation>Anulo çdo ndryshim dhe dil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>Cancel</source>
        <translation>Anuloje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="533"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Alt+N</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Apply any changes</source>
        <translation>Zbato çfarëdo ndryshimi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="555"/>
        <location filename="mainwindow.cpp" line="71"/>
        <location filename="ui_mainwindow.h" line="446"/>
        <source>Apply</source>
        <translation>Zbatoje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>Grub-i po instalohet në pajisjen %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="216"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="396"/>
        <location filename="mainwindow.cpp" line="419"/>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Error</source>
        <translation>Gabim</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Could not create a temporary folder</source>
        <translation>S’u krijua një dosje e përkohshme</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>S’u ujdis dot mjedis chroot.
Ju lutemi, rikontrolloni vendndodhjen e përzgjedhur.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>Kartela e formësimit GRUB (grub.cfg) po rikrijohet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Po kopjeruhet MBR apo PBR që nga pajisja %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Warning</source>
        <translation>Kujdes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>You are going to write the content of </source>
        <translation>Do të shkruani lëndën e </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source> to </source>
        <translation> te </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>

Are you sure?</source>
        <translation>

Jeni i sigurt?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Po rikthehet MBR/PBR prej kopjeruajtje te pajisja %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>S’u gjet dot pjesë sistemi EFIn (ESP) në ndonjë nga disqet. Ju lutemi, krijoni një ESP dhe riprovoni.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Select %1 location:</source>
        <translation>Përzgjidhni vendndodhje %1:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Back</source>
        <translation>Mbrapsht</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Success</source>
        <translation>Sukses</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Boot Repair?&lt;/b&gt;</source>
        <translation>Procesi përfundoi me sukses.&lt;p&gt;&lt;b&gt;Doni të dilet nga Riparim Nisjesh?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="396"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>Procesi përfundoi. Ndodhën gabime.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Jepni fjalëkalimin për të shkyçur pjesën e fshehtëzuar %1:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Na ndjeni, s’u hap dot kontejneri LUKS %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Select GRUB location</source>
        <translation>Përzgjidhni vendndodhje Grub-i</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Select Item to Back Up</source>
        <translation>Përzgjidhni Objektin për Kopjeruajtje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Select Item to Restore</source>
        <translation>Përzgjidhni Objektin për Rikthim</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>Select backup file name</source>
        <translation>Përzgjidhni emër kartele kopjeruajtje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>No file was selected.</source>
        <translation>S’u përzgjodh kartelë.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Përzgjidhni kartelë kopjeruajtje MBR-i ose PBR-i</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="550"/>
        <source>About %1</source>
        <translation>Mbi %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Simple boot repair program for antiX Linux</source>
        <translation>Program i thjeshtë riparimi nisjesh për antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="554"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Të drejta kopjimi (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>%1 License</source>
        <translation>Licencë %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>%1 Help</source>
        <translation>Ndihmë për %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Na ndjeni, s’u çmontua dot pjesa %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencë</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Regjistër ndryshimesh</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Anuloje</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Mbylle</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Error</source>
        <translation>Gabim</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Duket të keni hyrë si rrënjë, ju lutemi, që të përdorni këtë program, dilni dhe bëni hyrjen si përdorues normal.</translation>
    </message>
</context>
</TS>