<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="ui_mainwindow.h" line="392"/>
        <source>Boot Repair</source>
        <translation>Käynnistyksen korjaus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="ui_mainwindow.h" line="393"/>
        <source>Boot Repair is a utility that can be used to reinstall GRUB bootloader on the ESP (EFI System Partition), MBR (Master Boot Record) or root partition. It provides the option to reconstruct the GRUB configuration file and to back up and restore MBR or PBR (root).</source>
        <translation>Käynnistyskorjain on apuväline jota voidaan käyttää GRUB-käynnistyslataimen uudelleenasentamista vasten ESP:ssä (EFI-järjestelmäosio), MBR:ssä (Master Boot Record-pääkäynnistysio) tai root- eli juuriosiossa. Se tarjoaa uudelleenrakentamisen vaihtoehdon GRUB-asetustiedostolle sekä varmuuskopioidaksesi ja myös palauttaaksesi MBR:n tai PBR:n (root-juuri). </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <location filename="ui_mainwindow.h" line="394"/>
        <source>What would you like to do?</source>
        <translation>Mitä haluaisit tehdä?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <location filename="ui_mainwindow.h" line="395"/>
        <source>Backup MBR or PBR (legacy boot only)</source>
        <translation>Varmuuskopioi MBR tai PBR (ainoastaan vanhoillinen legacy-käynnistys)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <location filename="ui_mainwindow.h" line="396"/>
        <source>Reinstall GRUB bootloader on ESP, MBR or PBR (root)</source>
        <translation>Uudelleenasenna GRUB-käynnistyslataaja ESP:hen, MBR:ään tai PBR:ään (root-juuri)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="ui_mainwindow.h" line="397"/>
        <source>Repair GRUB configuration file</source>
        <translation>Korjaa GRUB-asetustiedosto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="ui_mainwindow.h" line="398"/>
        <source>Restore MBR or PBR from backup (legacy boot only)</source>
        <translation>Palauta MBR tai PBR varmuuskopiosta (ainoastaan vanhoillinen legacy-käynnistys)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <location filename="mainwindow.cpp" line="483"/>
        <location filename="ui_mainwindow.h" line="399"/>
        <source>Select Boot Method</source>
        <translation>Valitse käynnistystapa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="ui_mainwindow.h" line="401"/>
        <source>Master Boot Record</source>
        <translation>Pääkäynnistyslohko</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <location filename="ui_mainwindow.h" line="403"/>
        <source>MBR</source>
        <translation>MBR</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.ui" line="430"/>
        <location filename="ui_mainwindow.h" line="405"/>
        <location filename="ui_mainwindow.h" line="426"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="408"/>
        <source>Root (Partition Boot Record)</source>
        <translation>Root-juuri (osion käynnistystaltio)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <location filename="mainwindow.cpp" line="486"/>
        <location filename="ui_mainwindow.h" line="410"/>
        <source>root</source>
        <translation>juuri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="ui_mainwindow.h" line="411"/>
        <source>Install on:</source>
        <translation>Asenna:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <location filename="mainwindow.cpp" line="484"/>
        <location filename="ui_mainwindow.h" line="412"/>
        <source>Location:</source>
        <translation>Sijainti:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="277"/>
        <location filename="mainwindow.cpp" line="494"/>
        <location filename="ui_mainwindow.h" line="416"/>
        <source>Select root location:</source>
        <translation>Valitse root-juuren kohde:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="418"/>
        <source>EFI System Partition</source>
        <translation>EFI Järjestelmäosio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <location filename="ui_mainwindow.h" line="420"/>
        <source>ESP</source>
        <translation>ESP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <location filename="ui_mainwindow.h" line="422"/>
        <source>About this application</source>
        <translation>Tietoja tästä sovelluksesta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <location filename="ui_mainwindow.h" line="424"/>
        <source>About...</source>
        <translation>Tietoja...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="429"/>
        <source>Display help </source>
        <translation>Näytä ohje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <location filename="ui_mainwindow.h" line="431"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Cancel any changes then quit</source>
        <translation>Kumoa kaikki muutokset ja lopeta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="533"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Apply any changes</source>
        <translation>Hyväksy kaikki muutokset</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="555"/>
        <location filename="mainwindow.cpp" line="71"/>
        <location filename="ui_mainwindow.h" line="446"/>
        <source>Apply</source>
        <translation>Hyväksy</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>GRUB is being installed on %1 device.</source>
        <translation>Asennetaan GRUB laitteeseen %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="192"/>
        <location filename="mainwindow.cpp" line="216"/>
        <location filename="mainwindow.cpp" line="322"/>
        <location filename="mainwindow.cpp" line="396"/>
        <location filename="mainwindow.cpp" line="419"/>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="192"/>
        <source>Could not create a temporary folder</source>
        <translation>Ei voitu luoda väliaikaista kansiota</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Could not set up chroot environment.
Please double-check the selected location.</source>
        <translation>chroot-ympäristöä ei voitu rakentaa.
Tarkista valittu kohde.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>The GRUB configuration file (grub.cfg) is being rebuilt.</source>
        <translation>GRUB-asetustiedostoa (grub.cfg) uudelleenrakennetaan.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Backing up MBR or PBR from %1 device.</source>
        <translation>Varmuuskopioidaan MBR tai PBR laitteesta %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Warning</source>
        <translation>Varoitus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>You are going to write the content of </source>
        <translation>Aiot kirjoittaa sisällön</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source> to </source>
        <translation>kohteeseen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>

Are you sure?</source>
        <translation>

Oletko varma?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Restoring MBR/PBR from backup to %1 device.</source>
        <translation>Palautetaan MBR/PBR varmuuskopiosta laitteeseen %1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Could not find EFI system partition (ESP) on any system disks. Please create an ESP and try again.</source>
        <translation>EFI-järjestelmäosiota (ESP) ei löytynyt yhdeltäkään järjestelmälevyltä. Luo ESP ja yritä uudestaan.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Select %1 location:</source>
        <translation>Valitse %1 sijainti:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Back</source>
        <translation>Takaisin</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="391"/>
        <source>Success</source>
        <translation>Onnistui</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Process finished with success.&lt;p&gt;&lt;b&gt;Do you want to exit Boot Repair?&lt;/b&gt;</source>
        <translation>Toimenpide saatettiin onnistuneesti päätökseen. &lt;p&gt;&lt;b&gt; Haluatko sulkea käynnistyskorjaimen? &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="396"/>
        <source>Process finished. Errors have occurred.</source>
        <translation>Toimitus päättynyt. Ilmeni virheitä.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Enter password to unlock %1 encrypted partition:</source>
        <translation>Syötä salasana avataksesi %1 salauksella varustetun osion:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Sorry, could not open %1 LUKS container</source>
        <translation>Valitettavasti %1 LUKS säiliötä ei voitu avata</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Select GRUB location</source>
        <translation>Valitse GRUB:in sijainti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="505"/>
        <source>Select Item to Back Up</source>
        <translation>Valitse mitä varmuuskopioidaan</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Select Item to Restore</source>
        <translation>Valitse mitä palautetaan</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>Select backup file name</source>
        <translation>Valitse nimi varmuuskopiotiedostolle</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="525"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>No file was selected.</source>
        <translation>Ei tiedostoa valittuna.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="530"/>
        <source>Select MBR or PBR backup file</source>
        <translation>Valitse joko MBR tai PBR varmuuskopiotiedosto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="550"/>
        <source>About %1</source>
        <translation>%1 lisätietoja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>Versio: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="552"/>
        <source>Simple boot repair program for antiX Linux</source>
        <translation>Helppo käynnistyskorjainohjelma antiX Linux:ille</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="554"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>%1 License</source>
        <translation>%1 lupa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>%1 Help</source>
        <translation>%1 Apuopas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Sorry, could not mount %1 partition</source>
        <translation>Pahoittelut, osiota %1 ei voitu liittää</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Muutosloki</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Olet kirjautunut sisään pääkäyttäjänä. Kirjaudu sisään normaalina käyttäjänä käyttääksesi tätä ohjelmaa.</translation>
    </message>
</context>
</TS>