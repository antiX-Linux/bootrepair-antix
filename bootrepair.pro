#/*****************************************************************************
# * bootrepair.pro
# *****************************************************************************
# * Copyright (C) 2014 MX Authors
# *
# * Authors: Adrian
# *          MX Linux <http://mxlinux.org>
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * MX Boot Repair is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with MX Boot Repair.  If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

#-------------------------------------------------
#
# Project created by QtCreator 2014-04-02T18:30:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bootrepair
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp

HEADERS  += \
    version.h \
    mainwindow.h \
    about.h \
    cmd.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/bootrepair_ca.ts \
                translations/bootrepair_cs.ts \  
                translations/bootrepair_da.ts \
                translations/bootrepair_de.ts \
                translations/bootrepair_el.ts \
                 translations/bootrepair_es_ES.ts \
                translations/bootrepair_es.ts \
                translations/bootrepair_fil_PH.ts \
                translations/bootrepair_fi.ts \
		translations/bootrepair_fr_BE.ts \
                translations/bootrepair_fr.ts \
                translations/bootrepair_gl_ES.ts \
                translations/bootrepair_hi.ts \
                translations/bootrepair_hu.ts \
                translations/bootrepair_it.ts \
                translations/bootrepair_ja.ts \
                translations/bootrepair_nb.ts \
                translations/bootrepair_nl_BE.ts \
                translations/bootrepair_nl.ts \
                translations/bootrepair_pl.ts \
                translations/bootrepair_pt.ts \
                translations/bootrepair_pt_BR.ts \
                translations/bootrepair_ru.ts \
                translations/bootrepair_sl.ts \
                translations/bootrepair_sq.ts \
                translations/bootrepair_sv.ts \ 
                translations/bootrepair_tr.ts

RESOURCES += \
    images.qrc

